#This is the PSP for 99 Bottles 

#Entry Criteria
[1]Problem description
- Approach: Write code to pass the tests for 99 bottles

- 1.Fork this repo.
- 2.Ensure that you can build the code and run the tests. The repo has a single passing test.
- 3.Once you can build and run the code, set a two hour timer, and continue with the following steps.
- 4.Remove the '@Disabled("Remove this line to add this test.")' line from the next test in BottlesTest.Java
- 5.Write the code to pass the test enabled in step 4.
- 6.When the code passess all enabled tests, commit your changes. The commit message should name the test that was passed.
- 7.Return to step 4, until you pass all tests, or two hours have passed.

[2]GitLab repo 
link: https://gitlab.oit.duke.edu/fs144/fintech512-assignment2
Have already set up in gitlab

#Planning
- Issue: Do not know how to set up the background and how to make everything work to pass all the tests given.
- Verse: 2 hours
  - VerseNumber: Understanding: 1h
  - Code: 1h
- Song: 1 hour
- In total: 3 hours

#Development
- Set up the background: solved (2h)
- Make everything work + Understanding: 
  - Make everything work - solved (2h)
    - initial trial - 1h
    - debug - 1h
  - Understanding (30 mins)


#Testing:
- testFirstVerse: Passed - 10 mins
- testTheWholeSong: Passed - 3 mins
- TestAnotherVerse: Passed -1 mins
- TestAFewVerses: Passed -1 mins
- TestACoupleVerses: Passed - 2 mins
- TestVerse0: Passed - 1 mins
- TestVerse1: Passed - 1 mins
- TestVerse2: passed - 1 mins

#Evaluation:
- Understand and start the project
- Total time: 8 hours